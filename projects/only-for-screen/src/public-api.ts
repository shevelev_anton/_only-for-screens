/*
 * Public API Surface of only-for-screen
 */

export * from './lib/only-for-screen.module';
export * from './lib/only-for-screen.directive';
